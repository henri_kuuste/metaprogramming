## The Great Metaprogramming Conundrum
<br>
Henri Kuuste, 2017

==============================================================================

### WTF is metaprogramming?

----

### The art of turning this...
```c++
/**
 * Really nice comments.
 */
void do_things(Rabbit_t* bunnies, int num_bunnies) {
	for(int i = 0; i < num_bunnies; i++) {
		make_fluffy(bunnies[i]);
		make_pink(bunnies[i]);
	}
}
```

----

### ...into this!
```c++
/**
 * Garbage about `type_traits` and `std::allocator`.
 */
template<typename T, size_t N, typename Allocator,
	std::enable_if_t<std::is_happy<T>::value, void>* d = nullptr>
auto do_things(const Sequence<T>& bunnies)
  -> std::array<typename decltype(std::add_evil<T>{}), N>
{
	return std::move(
		hell::transform(bunnies, [&](const auto& b) {
			return Allocator::summon(hell::demonize<higher>(b));
	  })
	);
}
```
-----

![](sources/evilbunny.jpg)

==============================================================================

## Metaprogramming

Machines making machines

=====

## Offline metaprogramming

----

### Build scripts

```cmake
if(NOT STM32_FAMILY)
    string(REGEX REPLACE
      "^[sS][tT][mM]32(([fF][0-47])|([lL][0-1])|([tT])|([wW])).+$"
      "\\1" STM32_FAMILY ${STM32_CHIP_TYPE})
    string(TOUPPER ${STM32_FAMILY} STM32_FAMILY)
endif()

if(STM32_FAMILY STREQUAL "F4")
    list(APPEND CMSIS_COMMON_HEADERS core_cm4.h)
    # ...
elseif(STM32_FAMILY STREQUAL "F7")
    list(APPEND CMSIS_COMMON_HEADERS core_cm7.h)
    # ...
endif()
```

----

### Code generators

```erb
int main() {
<% env[:k].times do |k| %>
#if defined(METABENCH)
    constexpr std::array<int, <%= n %>> <%= "xs#{k}" %> = {{
        <%= ((k*n+1)..(k*n+n)).to_a.join(', ') %>
    }};
#endif
<% end %>
}
```

-----

### IDL

```xml
  <device platform="stm32" family="f4" name="405|407|415|417"
          pin_id="i|o|r|v|z" size_id="e|g">
    <driver type="gpio" name="stm32">
      <gpio device-pin-id="i|o|v|z" port="E" id="10">
        <af id="1" peripheral="Timer1" name="Channel2N"/>
        <af id="12" peripheral="Fsmc" name="D7"/>
      </gpio>
    </driver>
  </device>
```

```jinja
	enum class Port {
    %% for port in gpios | getPorts
		{{ port.name | upper }} = {{ port.name | letterToNum }},
    %% endfor
	};
```
----

### Preprocessor

```
#define tskIDLE_STACK_SIZE	configMINIMAL_STACK_SIZE

#if( configUSE_PREEMPTION == 0 )
  #define taskYIELD_IF_USING_PREEMPTION()
#else
  #define taskYIELD_IF_USING_PREEMPTION() portYIELD_WITHIN_API()
#endif
```

----

### Compiler

```c++
int square(int x) {
  return x*x;
}
```
<img style="background:none; border:none; box-shadow:none; margin: 0;" src="sources/down-arrow.png">

```llvm
define i32 square(i32 %x) local_unnamed_addr #0 {
entry:
  %mul = mul nsw i32 %x, %x
  ret i32 %mul
}
```
<img style="background:none; border:none; box-shadow:none; margin: 0;" src="sources/down-arrow.png">

```x86asm
square:
	imul  ecx, ecx
	mov   eax, ecx
	ret
```

=====

## Runtime metaprogramming

----

### Reflection

```python
def to_json(obj):
    members = []
    for member in obj.__dict__:
        members.append('"' + member + '" : ' +
                            str(getattr(obj, member)))
    return '{' + ', '.join(members) + '}'

print to_json(MyData)
```

----

### JIT (Just-in-time)
Runtime code injection

----

### Interpreted

```python
userCode = input("Type a function: ")
eval(userCode)
```

----

### Native

```c++
void* m = alloc_executable_memory(SIZE);
unsigned char code[] = {
  0x48, 0x89, 0xf8,        // mov %rdi, %rax
  0x48, 0x83, 0xc0, 0x04,  // add $4, %rax
  0xc3                     // ret
};
memcpy(m, code, sizeof(code));
long (*)(long) func = m;
int result = func(2);
```
==============================================================================

### Why do we care?
* Write ______ code:
  * <!-- .element: class="fragment" data-fragment-index="1" --> <span style="color: #8acae6; font-weight:bold;">less</span> <!-- .element: class="fragment" data-fragment-index="1" -->
  * <!-- .element: class="fragment" data-fragment-index="2" --> <span style="color: #e68a8a; font-weight:bold;">faster</span> <!-- .element: class="fragment" data-fragment-index="2" -->
  * <!-- .element: class="fragment" data-fragment-index="3" --> <span style="color: #b8e68a; font-weight:bold;">safer</span> <!-- .element: class="fragment" data-fragment-index="3" -->
  * <!-- .element: class="fragment" data-fragment-index="4" --> <span style="color: #ebe3aa; font-weight:bold;">smaller</span> <!-- .element: class="fragment" data-fragment-index="4" -->
  * <!-- .element: class="fragment" data-fragment-index="5" --> <span style="color: #c58ae6; font-weight:bold;">readable</span> <!-- .element: class="fragment" data-fragment-index="5" --> <span style="color: #e68a8a;">... wait, what?!?</span> <!-- .element: class="fragment" data-fragment-index="6" -->

-----

### The trouble with code generators
- <!-- .element: class="fragment" data-fragment-index="1" --> <span style="color: #ecd078; font-weight:bold;">Extra tools</span> <!-- .element: class="fragment" data-fragment-index="1" -->
  - Setup time <!-- .element: class="fragment" data-fragment-index="1" -->
  - Maintenance <!-- .element: class="fragment" data-fragment-index="1" -->
  - Knowledge <!-- .element: class="fragment" data-fragment-index="1" -->
- <!-- .element: class="fragment" data-fragment-index="2" --> <span style="color: #d95b43; font-weight:bold;">Editors</span> <!-- .element: class="fragment" data-fragment-index="2" -->
  - No code insight <!-- .element: class="fragment" data-fragment-index="2" -->
  - No formatting <!-- .element: class="fragment" data-fragment-index="2" -->
  - No <!-- .element: class="fragment" data-fragment-index="2" --> <span style="color:#a8caba;">syntax</span> <!-- .element: class="fragment" data-fragment-index="2" --> <span style="color:#ebe3aa; font-weight:bold;">highlighting</span> <!-- .element: class="fragment" data-fragment-index="2" -->
- <!-- .element: class="fragment" data-fragment-index="3"--> <span style="color: #e68a8a; font-weight:bold;">Debugging</span> <!-- .element: class="fragment" data-fragment-index="3"-->
-----

So we want to use
## **The <span style="color:#b8e68a;">Compiler</span>**

with some help from
#### <span style="color:#859c91;">the preprocessor</span>

=====

### Template Metaprogramming (TMP)
Best known from **<span style="color:#ebe3aa;">C++</span>**

But also D, Curl, XL, Lisp, Python, Nil, Groovy, Ruby, ...

-----

### The accident
by Erwin Unruh (1994)

```
unruh.cpp 30: conversion from enum to D<2> requested in Prime_print
unruh.cpp 30: conversion from enum to D<3> requested in Prime_print
unruh.cpp 30: conversion from enum to D<5> requested in Prime_print
unruh.cpp 30: conversion from enum to D<7> requested in Prime_print
unruh.cpp 30: conversion from enum to D<11> requested in Prime_print
unruh.cpp 30: conversion from enum to D<13> requested in Prime_print
unruh.cpp 30: conversion from enum to D<17> requested in Prime_print
unruh.cpp 30: conversion from enum to D<19> requested in Prime_print
```

-----

### Topics
- Generic programming
- `type_traits`
- SFINAE
- Concepts
- `constexpr`
- Reflection
- Tag dispatch
- Recursive and variadic templates
- Template specialization
- Expression templates

==============================================================================

# LIVE CODING
<!-- .slide: data-background="sources/f49ce472e55606fecad01b74bf041653.jpg" -->